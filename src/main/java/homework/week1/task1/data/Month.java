package homework.week1.task1.data;

import java.util.HashMap;
import java.util.Map;

public enum  Month {
    Unknown,
    January, February,
    March, April, May,
    June, July, August,
    September, October, November,
    December;

    public int getMonthNumber(){
        return ordinal();
    }

    private static Map<Integer, Month> monthNumberMap = new HashMap<>();

    static {
        for (int i = 1; i < Month.values().length; i++){
            monthNumberMap.put(i, Month.values()[i]);
        }
    }

    public static Month getMonthByNumber(int monthNumber){
        if (monthNumber > 0 && monthNumber <= 12) {
            return monthNumberMap.get(monthNumber);
        }

        throw new IllegalArgumentException("Wrong month number");
    }
}
