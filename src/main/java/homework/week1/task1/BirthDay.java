package homework.week1.task1;

import homework.week1.task1.data.Month;

public class BirthDay {
    private int day;
    private Month month;
    private int year;

    public BirthDay(int day, int month, int year) {
        this.day = day;
        this.month = Month.getMonthByNumber(month);
        this.year = year;
    }

    public BirthDay(int day, Month month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
