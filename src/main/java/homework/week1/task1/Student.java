package homework.week1.task1;

import homework.week1.task1.data.Sex;

import java.util.ArrayList;
import java.util.List;

public class Student {
    protected String name;
    protected Sex sex;
    protected BirthDay birthDay;
    protected int age;

    protected List<String> subjectsList = new ArrayList<>();
    protected int averageMark;

    public Student(){}

    public Student(final String name){
        this.name = name;
    }

    public Student(String name, BirthDay birthDay) {
        this.name = name;
        this.birthDay = birthDay;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name != null && !name.equals("")) {
            this.name = name;
        }
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age >= 14 && age <= 100) {
            this.age = age;
        }
    }

    public boolean addSubject(final String subject){
        return subjectsList.add(subject);
    }

    public boolean removeSubject(final String subject){
        return subjectsList.remove(subject);
    }

    public static Student initStudentByNameAndSex(final String name, final Sex sex){
        Student student = new Student();
        student.setName(name);
        student.setSex(sex);

        return student;
    }

    @Override
    public String toString() {
        return String.format("Student: name => [%s], age => [%d], sex => [%s]", name, age, sex);
    }
}
