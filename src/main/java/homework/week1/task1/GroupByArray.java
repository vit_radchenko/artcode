package homework.week1.task1;

import java.util.Arrays;

public class GroupByArray {
    private static final int DEFAULT_LENGTH = 2;

    private String name;
    private Student[] studentList;
    private int counter = 0;

    public GroupByArray(final String name){
        this.name = name;
        studentList = new Student[DEFAULT_LENGTH];
    }

    public GroupByArray(final String name, final int size){
        this.name = name;
        studentList = new Student[size];
    }

    public int getSize(){
        return counter;
    }

    public boolean addStudent(final Student student){
        if (student == null || isStudentPresent(student)) return Boolean.FALSE;

        //resize an array
        if (counter >= studentList.length) {
            studentList = Arrays.copyOf(studentList, (int) (studentList.length * 1.5 + 1));
        }

        studentList[counter++] = student;
        return Boolean.TRUE;
    }

    public boolean removeStudent(final String name){
        if (name == null || name.isEmpty()) return Boolean.FALSE;

        for (int i = 0; i < counter; i++){
            if (name.equals(studentList[i].getName())){
                System.arraycopy(studentList, i + 1, studentList, i, studentList.length - (i + 1));
                counter--;
                return Boolean.TRUE;
            }
        }

        return Boolean.FALSE;
    }

    public boolean removeStudent(final Student student){
        if (student == null) return Boolean.FALSE;

        for (int i = 0; i < counter; i++){
            if (student.equals(studentList[i])){
                System.arraycopy(studentList, i + 1, studentList, i, studentList.length - (i + 1));
                counter--;
                return Boolean.TRUE;
            }
        }

        return Boolean.FALSE;
    }

    public GroupByArray sort(){
        for (int i = 0; i < counter; i++){
            String outName = studentList[i].getName();

            for (int j = i + 1; j < counter; j++){
                String inName = studentList[j].getName();

                if (outName.compareTo(inName) > 0){
                    Student tempStudent = studentList[i];
                    studentList[i] = studentList[j];
                    studentList[j] = tempStudent;
                }

            }
        }

        return this;
    }

    public Student search(final String name){
        if (name == null) throw new IllegalArgumentException("Name is null");

        for (int i = 0; i < counter; i++){
            if (name.equals(studentList[i].getName())){
                return studentList[i];
            }
        }

        return null;
    }

    public boolean isStudentPresent(final String name){
        if (name == null) return false;

        for (int i = 0; i < counter; i++){
            if (name.equals(studentList[i].getName())){
                return Boolean.TRUE;
            }
        }

        return Boolean.FALSE;
    }

    public boolean isStudentPresent(final Student student){
        if (student == null) return false;

        for (int i = 0; i < counter; i++){
            if (student.equals(studentList[i])){
                return Boolean.TRUE;
            }
        }

        return Boolean.FALSE;
    }

    public String showGroup(){
        StringBuilder stringBuilder = new StringBuilder(String.format("Group[%s]:\n", name));

        for (int i = 0; i < counter; i++){
            stringBuilder.append(studentList[i].toString());
            stringBuilder.append("\n");
        }

        return stringBuilder.toString();
    }

}
