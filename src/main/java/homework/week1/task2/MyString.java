package homework.week1.task2;


import java.util.Arrays;

public class MyString {
    private char[] charArray;

    public MyString(String str) {
        charArray = str.toCharArray();
    }

    public MyString(char[] charArray) {
        this.charArray = charArray;
    }

    public MyString toLowerCase() {
        char[] temp = Arrays.copyOf(charArray, charArray.length);

        for (int i = 0; i < temp.length; i++) {
            if (temp[i] >= 65 && temp[i] <= 90) {
                temp[i] = (char) (temp[i] + 32);
            }
        }

        return new MyString(temp);
    }

    public MyString toUpperCase() {
        char[] temp = Arrays.copyOf(charArray, charArray.length);

        for (int i = 0; i < temp.length; i++) {
            if (temp[i] >= 97 && temp[i] <= 122) {
                temp[i] = (char) (temp[i] - 32);
            }
        }
        return new MyString(temp);
    }

    public MyString concat(MyString string) {
        char[] anotherCharArray = string.charArray;
        char[] temp = new char[charArray.length + anotherCharArray.length];

        System.arraycopy(charArray, 0, temp, 0, charArray.length);
        System.arraycopy(anotherCharArray, 0, temp, charArray.length, anotherCharArray.length);

        charArray = temp;
        return this;
    }

    public boolean contains(char[] anotherCharArray) {
        if (anotherCharArray.length > charArray.length) return Boolean.FALSE;

        for (int i = 0, j = anotherCharArray.length; j <= charArray.length; i++, j++) {
            char[] temp = substring(i, j).toCharArray();
            if (Arrays.equals(anotherCharArray, temp)) {
                return Boolean.TRUE;
            }
        }

        return Boolean.FALSE;
    }

    public boolean contains(MyString anotherString) {
        if (anotherString.length() > charArray.length) return Boolean.FALSE;
        return contains(anotherString.charArray);
    }

    public MyString trim() {
        int startIndex = 0;
        while (charArray[startIndex] == ' ') {
            startIndex++;
        }

        int endIndex = charArray.length - 1;
        while (charArray[endIndex] == ' ') {
            endIndex--;
        }

        return substring(startIndex, endIndex);
    }

    public MyString substring(int indexFrom) {
        return substring(indexFrom, charArray.length);
    }

    public MyString substring(int indexFrom, int indexTo) {
        int length = indexTo - indexFrom;
        char[] subString = new char[length];

        System.arraycopy(charArray, indexFrom, subString, 0, length);
        return new MyString(subString);
    }

    public void display() {
        for (int i = 0; i < charArray.length; i++) {
            System.out.print(charArray[i]);
        }
        System.out.println();
    }

    public int indexOf(char value) {
        for (int i = 0; i < charArray.length; i++) {
            if (charArray[i] == value) {
                return i;
            }
        }

        return -1;
    }


    public boolean equals(MyString anotherString) {
        return Arrays.equals(this.charArray, anotherString.charArray);
    }

    private int length() {
        return charArray.length;
    }

    public char[] toCharArray() {
        return charArray;
    }

    @Override
    public String toString() {
        return new String(charArray);
    }
}