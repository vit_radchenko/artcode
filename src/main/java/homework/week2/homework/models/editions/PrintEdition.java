package homework.week2.homework.models.editions;

import homework.week2.homework.models.Author;

public class PrintEdition extends Edition {

    public PrintEdition(String name, Author author) {
        super(name, author);
    }

    public PrintEdition(String name, String authorName, String authorLastName) {
        super(name, authorName, authorLastName);
    }

}
