package homework.week2.homework.models.editions;

import homework.week2.homework.models.Author;

public class DigitalEdition extends Edition {

    public DigitalEdition(String name, Author author) {
        super(name, author);
    }

    public DigitalEdition(String name, String authorName, String authorLastName) {
        super(name, authorName, authorLastName);
    }

}
