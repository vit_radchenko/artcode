package homework.week2.homework;


import homework.week2.homework.models.Author;
import homework.week2.homework.models.editions.Edition;
import homework.week2.homework.models.editions.PrintEdition;

import java.util.*;

public class Library {
    // begin of Singleton
    private volatile static Library library;

    private Library(){}

    public static Library getLibrary(){
        if (library == null){
            synchronized (Library.class) {
                if (library == null) {
                    library = new Library();
                }
            }
        }

        return library;
    }

    //end of Singleton

    public List<Edition> editionsList = new ArrayList<>();
    public List<Reader> readersList = new ArrayList();
    public List<Author> authorList = new ArrayList<>();
    public List<Reader> blackList = new ArrayList<>();

    public Map<UUID, Edition> editionsMap = new HashMap<>();
    public Map<UUID, Reader> readerMap = new HashMap<>();

    public static void main(String[] args){
        Library library = new Library();

        library.showReaders();
        library.showPrintEdition();
    }

    // 1) посмотреть список читателей
    public void showReaders(){
        Collections.sort(readersList);

        for (Reader reader: readersList){
            System.out.println(reader.toString() + "\n");
        }
    }

    // 2) посмотреть список доступных конкретных печатных изданий
    public void showPrintEdition(){
        Collections.sort(editionsList);

        for (Edition edition: editionsList){
            if (edition instanceof PrintEdition && edition.getQuantity() != 0 && edition.isAvailable()){
                System.out.println(edition.toString() + "\n");
            }
        }
    }

    // 3) добавить печатное издание в библиотеку
    public void addPrintEdition(String name, String authorName, String authorLastName){
        Edition editionToAdd = new PrintEdition(name, authorName, authorLastName);

        if (editionsList.contains(editionToAdd)){
            Edition existEdition = editionsList.get(editionsList.indexOf(editionToAdd));
            existEdition.increaseQuantity();
        } else {
            editionsList.add(editionToAdd);
        }
    }

    // 4) добавить читателя в список читателей
    public void addReader(String name){
        Reader reader = new Reader(name);
        if (!readersList.contains(reader)) {
            readersList.add(reader);
        }
    }

    public Author addAuthor(String name, String lastName){
        Author author = new Author(name, lastName);
        return addAuthor(author);
    }

    public Author addAuthor(Author author){
        if (!authorList.contains(author)){
            authorList.add(author);
        } else {
            author = authorList.get(authorList.indexOf(author));
        }

        return author;
    }

    //5) выдать печатное издание читателю (если книга есть в наличии).
    public boolean givePrintEdition(Reader reader, PrintEdition edition){
        if (blackList.contains(reader)) return false;
        if (reader.getTakenPrintedEditionsList().contains(edition) || reader.getTakenPrintedEditionsList().size() > 3) return false;
        if (editionsList.get(editionsList.indexOf(edition)).getQuantity() == 0) return false;

        reader.takeEdition(edition);
        edition.addHolder(reader);
        edition.decreaseQuantity();

        return true;
    }

    // 6) посмотреть список печатных изданий, которые находятся у читателей
    public void showTakenPrintedEdition(){
        for (Edition edition: editionsList){
            if (edition instanceof PrintEdition && edition.getHolderListSize() != 0){
                System.out.println(edition.toString() + "\n");
            }
        }
    }

    // 7) посмотреть список печатных изданий, которые находятся у конкретного читателя
    public void showPrintEditionForReader(Reader reader){
        List<Edition> editions = reader.getTakenEditionsList();
        for (Edition edition: editions){
            if (edition instanceof PrintEdition){
                System.out.println(edition.toString());
            }
        }
    }

    // 8) добавить читателя в черный список (ему нельзя выдавать печатные издания)
    public void addToBlackList(Reader reader){
        blackList.add(reader);
    }

//    9) посмотреть печатные издания конкретного автора
    public void showPrintedEditionOfAuthor(Author author){
        for (Edition edition: author.getEditions()){
            if (edition instanceof PrintEdition){
                System.out.println(edition.toString());
            }
        }
    }

//    10) посмотреть печатные издания конкретного автора???
//    11) посмотреть печатные издания конкретного года
    public void showPrintedEditionOfYear(int year){
        for (Edition edition: editionsList){
            if (edition instanceof PrintEdition && edition.getYear() == year){
                System.out.println(edition.toString());
            }
        }
    }
//    12) найти печатное издание по названию (ключевым словам)
    public PrintEdition findPrintEdition(String keyWords){
        for (Edition edition: editionsList){
            if (edition instanceof  PrintEdition){
                if (edition.toString().contains(keyWords)){
                    return (PrintEdition) edition;
                }
            }
        }

        return null;
    }


    public static Reader findReaderByUUID(UUID uuid){
        Map<UUID, Reader> readerMap = getLibrary().readerMap;
        return readerMap.get(uuid);
    }
}
